# Holiday Romcom Bingo

# What Is This?
An app that makes [bingo](https://en.wikipedia.org/wiki/Bingo_(American_version)) cards containing clichés common in holiday-themed romantic comedies, usually produced and aired 24/7 by a cable channel known for selling greeting cards.

Classic bingo commonly is played with 75 balls, so this app contains 75 clichés.

# How to Use
* Go to the [page](https://aastaneh.gitlab.io/holiday-romcom-bingo/) to generate a card. Refresh the page to generate new ones. Print the ones you like!
* Play a random film from the genre.
* Enjoy!

# How Can I Edit/Modify the Clichés?
File tropes.js has an array containing them all. Feel free to submit a merge request!

# Credits
 * [Cult of the Party Parrot](https://cultofthepartyparrot.com/) for the holiday parrot gif
 * Websites where I got inspiration: [1](https://www.cosmopolitan.com/entertainment/movies/a30001084/classic-christmas-movie-tropes-scenes/) [2](https://www.stylist.co.uk/entertainment/film/christmas-romcom-movie-tropes/574942) [3](https://www.fandango.com/movie-photos/theyre-cheesy-theyre-corny-theyre-holiday-movie-cliches-we-love-640) [4](https://www.mattressonline.co.uk/blog/sleep-news/rom-com-cliches/)

