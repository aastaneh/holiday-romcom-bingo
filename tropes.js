const tropes = [
"lead is in the media",
"lead is a bigshot from the city",
"lead just lost her job",
"lead is newcomer in a small town",
"lead returns to their hometown",
"big difference between the couple",
"trouble with finances or family business",
"workaholism",
"rekindled romance",
"some form of Christmas 'magic'",
"big holiday event (contest, festival, party)",
"cooking scene (baking)",
"search for a Christmas tree",
"decorating the Christmas tree",
"jealous ex, friend, or coworker jeopardizing romance",
"heart to heart between lovers",
"close contact under mistletoe",
"communication mix-up affects the couple",
"learning about Hanukkah",
"snowing",
"posh furnished luxury apartment or mansion",
"ice skating/winter sports",
"death/illness of character or family member",
"caroling or singalong",
"a musical number",
"character is unlucky or clumsy",
"breakup w/ deadbeat or emotionally distant partner",
"red flag behavior shown as 'romantic'",
"color coordinated outerwear",
"cozy lit fireplace",
"big meal with family or friends",
"ringing bells",
"character hates the holidays (humbug!)",
"'just friends' or fake relationship",
"mountain town",
"coastal town",
"plot contains time travel or alternate life",
"incompatable couple supplies love interest for lead",
"random celebrity cameo",
"Santa, elves, or other magical holiday figures",
"impossibly loving/doting relatives",
"impossibly difficult potential inlaws",
"quirky friend",
"advent calendar",
"uggs/leggings",
"adorable dog",
"wise, mysterious elder figure",
"religious-themed jewelry",
"nativity scene",
"impossibly well-behaved kids",
"beige attire",
"love interest is a jerk at first",
"marriage proposal / wedding",
"call/meeting w/ very professional manager/boss",
"more than one couple gets together",
"'meet-cute' scene at/near a cafe or a bookstore",
"competition over holiday decorations",
"elderly says something weird",
"unexpected royalty",
"big secret gets revealed",
"career aspirations as part of the plot",
"reference to New York or 'big city'",
"meddling family",
"cheating",
"makeover",
"scene at an airport",
"horses or reindeer",
"giving up career for romantic interest",
"reformed 'bad boy'",
"sentimental gift-giving",
"hot cocoa",
"work deadline on Christmas Eve",
"romantic interest divorced or grieving",
"almost-but-not-quite kissing",
"grand romantic gesture"
];

// Now we need to select 24 random elements from the array, and randomize the list
const shuffled = tropes.sort(() => 0.5 - Math.random());
let selected = shuffled.slice(0, 24);

// then write the free square
selected.splice(12, 0, "<b>FREE SQUARE</b>");

// now, we need to constuct a 5x5 html table.
let card = "<table><tr>";
let i = 0;
while (i < selected.length) {
  // new row
  if ( (i % 5) == 0 && i > 0) {
    card +="</tr><tr>";
  }
  card += "<td>" + selected[i] + "</td>";
  i++;
}
card += "</tr></table>";

// write the table to to the bingo-card div
document.getElementById("bingo-card").innerHTML = card;

